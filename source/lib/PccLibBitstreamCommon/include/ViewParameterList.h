/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2017, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef VIEWPARAMETERSLIST_H
#define VIEWPARAMETERSLIST_H

#include "PCCBitstreamCommon.h"
#include "CameraExtrinsics.h"
#include "CameraIntrinsics.h"
#include "DepthQuantization.h"
#include "PruningParents.h"

namespace pcc {
// Specified in ISO/IEC 23090-12
// 7.3.6.3.3	MIV view parameters list syntax
class ViewParametersList {
 public:
   ViewParametersList() :
     numViewsMinus1_(0),
     viewEnabledPresentFlag_(false),
     explicitViewIdFlag_(false),
     intrinsicParametersEqualFlag_(false),
     depthQuantizationParametersEqualFlag_(false),
     pruningGraphParametersPresentFlag_(false) {
     viewEnabledInAtlasFlag_.clear();
     viewCompleteInAtlasFlag_.clear();
     viewId_.clear();
     cameraExtrinsicsList_.clear();
     cameraIntrinsicsList_.clear();
     depthQuantizationList_.clear();
     pruningParentsList_.clear();
   }
   ~ViewParametersList() {
     viewEnabledInAtlasFlag_.clear();
     viewCompleteInAtlasFlag_.clear();
     viewId_.clear();
     cameraExtrinsicsList_.clear();
     cameraIntrinsicsList_.clear();
     depthQuantizationList_.clear();
     pruningParentsList_.clear();
   }

	ViewParametersList& operator=( const ViewParametersList& ) = default;
	
  uint32_t                        getNumViewsMinus1() { return numViewsMinus1_; }
  void                            setNumViewsMinus1( uint32_t value ) { numViewsMinus1_ = value; }
  bool                            getViewEnabledPresentFlag() { return viewEnabledPresentFlag_; }
  void                            setViewEnabledPresentFlag( bool value ) { viewEnabledPresentFlag_ = value; }
  std::vector<std::vector<bool>>& getViewEnabledInAtlasFlagList() { return viewEnabledInAtlasFlag_; }
  std::vector<std::vector<bool>>& getViewCompleteInAtlasFlagList() { return viewCompleteInAtlasFlag_; }
  bool     getExplicitViewIdFlag() { return explicitViewIdFlag_; }
  std::vector<uint16_t>& getViewIdList() { return viewId_; }
  void     setExplicitViewIdFlag(bool value) { explicitViewIdFlag_ = value; }
  std::vector<CameraExtrinsics>& getCameraExtrinsicList() { return cameraExtrinsicsList_; }
  bool     getIntrinsicParametersEqualFlag() { return intrinsicParametersEqualFlag_; }
  void     setIntrinsicParametersEqualFlag(bool value) { intrinsicParametersEqualFlag_ = value; }
  std::vector<CameraIntrinsics>& getCameraIntrinsicList() { return cameraIntrinsicsList_; }
  bool     getDepthQuantizationParametersEqualFlag() { return depthQuantizationParametersEqualFlag_; }
  void     setDepthQuantizationParametersEqualFlag(bool value) { depthQuantizationParametersEqualFlag_ = value; }
  std::vector<DepthQuantization>& getDepthQuantizationList() { return depthQuantizationList_; }
  bool     getPruningGraphParametersPresentFlag() { return pruningGraphParametersPresentFlag_; }
  void     setPruningGraphParametersPresentFlag(bool value) { pruningGraphParametersPresentFlag_ = value; }
  std::vector<PruningParents>& getPruningParentsList() { return pruningParentsList_; }
 private:
  uint16_t                       numViewsMinus1_;
  bool                           viewEnabledPresentFlag_;
   std::vector<std::vector<bool>> viewEnabledInAtlasFlag_;
   std::vector<std::vector<bool>> viewCompleteInAtlasFlag_;
   bool                           explicitViewIdFlag_;
   std::vector<uint16_t>          viewId_;
   std::vector<CameraExtrinsics>  cameraExtrinsicsList_;
   bool                           intrinsicParametersEqualFlag_; 
   std::vector<CameraIntrinsics>  cameraIntrinsicsList_;
   bool                           depthQuantizationParametersEqualFlag_; 
   std::vector<DepthQuantization> depthQuantizationList_;
   bool                           pruningGraphParametersPresentFlag_;
   std::vector<PruningParents>    pruningParentsList_;
};
};  // namespace pcc

#endif //~MIV_VIEWPARAMETERSLIST_H
