/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2017, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef PCC_BITSTREAM_CAMERAINTRINSICS_H
#define PCC_BITSTREAM_CAMERAINTRINSICS_H
#include "PCCBitstreamCommon.h"

namespace pcc {
// Specified in ISO/IEC 23090-12
// 7.3.6.3.8	Camera intrinsics syntax
class CameraIntrinsics {
 public:
  CameraIntrinsics() : 
		cameraType_ (0),
	  projectionPlaneWidthMinus1_ (0),
		projectionPlaneHeightMinus1_ (0),
		//reversedDepthFlag_(false),
	  erpPhiMin_ (0),
		erpPhiMax_ (0),
	  erpThetaMin_ (0),
		erpThetaMax_ (0),
	  //cubicMapType_ (0),
		perspectiveFocalHor_ (0),
	  perspectiveFocalVer_ (0),
		perspectiveCenterHor_ (0),
		perspectiveCenterVer_ (0)	{ 
	}
  ~CameraIntrinsics() { 
	}

	CameraIntrinsics& operator=( const CameraIntrinsics& ) = default;
	bool operator==( const CameraIntrinsics &rhs ) const {
		if ((cameraType_ != rhs.cameraType_) || (projectionPlaneWidthMinus1_ != rhs.projectionPlaneWidthMinus1_) || (projectionPlaneHeightMinus1_ != rhs.projectionPlaneHeightMinus1_) )
			return false;
		else
		{
			if (cameraType_ == 0) {
				return ((rhs.erpPhiMin_ == erpPhiMin_) &&
					(rhs.erpPhiMax_ == erpPhiMax_) &&
					(rhs.erpThetaMin_ == erpThetaMin_) &&
					(rhs.erpThetaMax_ == erpThetaMax_));
			}
			else if (cameraType_ == 1) {
				return ((rhs.perspectiveFocalHor_== perspectiveFocalHor_) &&
					(rhs.perspectiveFocalVer_ == perspectiveFocalVer_) &&
					(rhs.perspectiveCenterHor_ == perspectiveCenterHor_) &&
					(rhs.perspectiveCenterVer_ == perspectiveCenterVer_));
			}
			else
				return false;
		};
	}
  bool operator!=(const CameraIntrinsics &other) const { return !operator==(other); };
	
  uint8_t getCameraType() { return cameraType_; }
  void setCameraType( uint8_t value ) { cameraType_ = value; }

  uint16_t getProjectionPlaneWidthMinus1() { return projectionPlaneWidthMinus1_; }
  void setProjectionPlaneWidthMinus1( uint16_t value ) { projectionPlaneWidthMinus1_ = value; }
  uint16_t getProjectionPlaneHeightMinus1() { return projectionPlaneHeightMinus1_; }
  void setProjectionPlaneHeightMinus1( uint16_t value ) { projectionPlaneHeightMinus1_ = value; }
  
  //bool getReversedDepthFlag() { return reversedDepthFlag_; }
  //void setReversedDepthFlag( bool value ) { reversedDepthFlag_ = value; }

	float getErpPhiMin() { return erpPhiMin_; }
  void setErpPhiMin( float value ) { erpPhiMin_ = value; }
  float getErpPhiMax() { return erpPhiMax_; }
  void setErpPhiMax( float value ) { erpPhiMax_ = value; }

  float getErpThetaMin() { return erpThetaMin_; }
  void setErpThetaMin( float value ) { erpThetaMin_ = value; }
  float getErpThetaMax() { return erpThetaMax_; }
  void setErpThetaMax( float value ) { erpThetaMax_ = value; }

  //uint32_t getCubicMapType() { return cubicMapType_; }
  //void setCubicMapType( uint32_t value ) { cubicMapType_ = value; }

  float getPerspectiveFocalHor() { return perspectiveFocalHor_; }
  void setPerspectiveFocalHor( float value ) { perspectiveFocalHor_ = value; }
  float getPerspectiveFocalVer() { return perspectiveFocalVer_; }
  void setPerspectiveFocalVer( float value ) { perspectiveFocalVer_ = value; }

  float getPerspectiveCenterHor() { return perspectiveCenterHor_; }
  void setPerspectiveCenterHor( float value ) { perspectiveCenterHor_ = value; }
  float getPerspectiveCenterVer() { return perspectiveCenterVer_; }
  void setPerspectiveCenterVer( float value ) { perspectiveCenterVer_ = value; }

  float getOrthoWidth() { return orthoWidth_; }
  void setOrthoWidth( float value ) { orthoWidth_ = value; }
  float getOrthoHeight() { return orthoHeight_; }
  void setOrthoHeight( float value ) { orthoHeight_ = value; }

 private:
	uint8_t                         cameraType_;
	uint16_t                        projectionPlaneWidthMinus1_;
	uint16_t                        projectionPlaneHeightMinus1_;
	//bool                            reversedDepthFlag_;
	float                           erpPhiMin_;
	float                           erpPhiMax_;
	float                           erpThetaMin_;
	float                           erpThetaMax_;
	//uint32_t                        cubicMapType_;
	float                           perspectiveFocalHor_;
	float                           perspectiveFocalVer_;
	float                           perspectiveCenterHor_;
	float                           perspectiveCenterVer_;
	float                           orthoWidth_;
	float                           orthoHeight_;
};
};  // namespace pcc

#endif //~PCC_BITSTREAM_CAMERAINTRINSICS_H
