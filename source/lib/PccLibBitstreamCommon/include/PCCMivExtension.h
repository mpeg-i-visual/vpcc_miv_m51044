/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2017, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef PCC_BITSTREAM_MIVEXTENSION_H
#define PCC_BITSTREAM_MIVEXTENSION_H

#include "PCCBitstreamCommon.h"
#include "ViewParameterList.h"
#include "ViewParametersUpdateExtrinsics.h"
#include "ViewParametersUpdateIntrinsics.h"
#include "ViewParametersUpdateDepthQuantization.h"
#include "PCCVUIParameters.h"


namespace pcc {

// Specified in ISO/IEC 23090-12

// 7.3.4.8 Packing information syntax syntax
class PackingInformation {
 public:
  PackingInformation() : 
      codecId_( 0 ), 
      regionsCountMinus1_(0) 
  {
    regionTileId_.clear();
    regionTypeIdMinus2_.clear();
    regionTopLeftX_.clear();
    regionTopLeftY_.clear();
    regionWidthMinus1_.clear();
    regionHeightMinus1_.clear();
    regionMapIndex_.clear();
    regionRotationFlag_.clear();
    regionAuxiliaryDataFlag_.clear();
    regionAttrTypeId_.clear();
    regionAttrPartitionsFlag_.clear();
    regionAttrPartitionIndex_.clear();
    regionAttrPartitionsMinus1_.clear();
  }
  ~PackingInformation() {
    regionTileId_.clear();
    regionTypeIdMinus2_.clear();
    regionTopLeftX_.clear();
    regionTopLeftY_.clear();
    regionWidthMinus1_.clear();
    regionHeightMinus1_.clear();
    regionMapIndex_.clear();
    regionRotationFlag_.clear();
    regionAuxiliaryDataFlag_.clear();
    regionAttrTypeId_.clear();
    regionAttrPartitionsFlag_.clear();
    regionAttrPartitionIndex_.clear();
    regionAttrPartitionsMinus1_.clear();
  }
  PackingInformation& operator=( const PackingInformation& ) = default;

  uint8_t getCodecId() { return codecId_; }
  void    setCodecId( uint8_t value ) { codecId_ = value; }
  size_t  getRegionsCountMinus1() { return regionsCountMinus1_; }
  void    setRegionsCountMinus1( size_t value ) { regionsCountMinus1_ = value; }
  void    allocateStructures(int size) {
    regionTileId_.resize(size);
    regionTypeIdMinus2_.resize( size );
    regionTopLeftX_.resize( size );
    regionTopLeftY_.resize( size );
    regionWidthMinus1_.resize( size );
    regionHeightMinus1_.resize( size );
    regionMapIndex_.resize( size );
    regionRotationFlag_.resize( size );
    regionAuxiliaryDataFlag_.resize( size );
    regionAttrTypeId_.resize( size );
    regionAttrPartitionsFlag_.resize( size );
    regionAttrPartitionIndex_.resize( size );
    regionAttrPartitionsMinus1_.resize( size );
  }
  uint8_t getRegionTileId(size_t idx) { return regionTileId_[idx]; }
  void    setRegionTileId( size_t idx, uint8_t value ) { regionTileId_[idx] = value; }
  uint8_t getRegionTypeIdMinus2( size_t idx ) { return regionTypeIdMinus2_[idx]; }
  void     setRegionTypeIdMinus2( size_t idx, uint8_t value ) { regionTypeIdMinus2_[idx] = value; }
  uint16_t getRegionTopLeftX( size_t idx ) { return regionTopLeftX_[idx]; }
  void     setRegionTopLeftX( size_t idx, uint16_t value ) { regionTopLeftX_[idx] = value; }
  uint16_t getRegionTopLeftY( size_t idx ) { return regionTopLeftY_[idx]; }
  void     setRegionTopLeftY( size_t idx, uint16_t value ) { regionTopLeftY_[idx] = value; }
  uint16_t getRegionWidthMinus1( size_t idx ) { return regionWidthMinus1_[idx]; }
  void     setRegionWidthMinus1( size_t idx, uint16_t value ) { regionWidthMinus1_[idx] = value; }
  uint16_t getRegionHeightMinus1( size_t idx ) { return regionHeightMinus1_[idx]; }
  void     setRegionHeightMinus1( size_t idx, uint16_t value ) { regionHeightMinus1_[idx] = value; }
  uint8_t  getRegionMapIndex( size_t idx ) { return regionMapIndex_[idx]; }
  void     setRegionMapIndex( size_t idx, uint8_t value ) { regionMapIndex_[idx] = value; }
  bool     getRegionRotationFlag( size_t idx ) { return regionRotationFlag_[idx]; }
  void     setRegionRotationFlag( size_t idx, bool value ) { regionRotationFlag_[idx] = value; }
  bool     getRegionAuxiliaryDataFlag( size_t idx ) { return regionAuxiliaryDataFlag_[idx]; }
  void     setRegionAuxiliaryDataFlag( size_t idx, bool value ) { regionAuxiliaryDataFlag_[idx] = value; }
  uint8_t  getRegionAttrTypeId( size_t idx ) { return regionAttrTypeId_[idx]; }
  void     setRegionAttrTypeId( size_t idx, uint8_t value ) { regionAttrTypeId_[idx] = value; }
  bool     getRegionAttrPartitionsFlag( size_t idx ) { return regionAttrPartitionsFlag_[idx]; }
  void     setRegionAttrPartitionsFlag( size_t idx, bool value ) { regionAttrPartitionsFlag_[idx] = value; }
  uint8_t  getRegionAttrPartitionIndex( size_t idx ) { return regionAttrPartitionIndex_[idx]; }
  void     setRegionAttrPartitionIndex( size_t idx, uint8_t value ) { regionAttrPartitionIndex_[idx] = value; }
  uint8_t  getRegionAttrPartitionsMinus1( size_t idx ) { return regionAttrPartitionsMinus1_[idx]; }
  void     setRegionAttrPartitionsMinus1( size_t idx, uint8_t value ) { regionAttrPartitionsMinus1_[idx] = value; }

 private:
  uint8_t codecId_;
  size_t  regionsCountMinus1_;
  std::vector<uint8_t> regionTileId_;
  std::vector<uint8_t> regionTypeIdMinus2_;
  std::vector<uint16_t> regionTopLeftX_;
  std::vector<uint16_t> regionTopLeftY_;
  std::vector<uint16_t> regionWidthMinus1_;
  std::vector<uint16_t> regionHeightMinus1_;
  std::vector<uint8_t>  regionMapIndex_;
  std::vector<bool> regionRotationFlag_;
  std::vector<bool>     regionAuxiliaryDataFlag_;
  std::vector<uint8_t> regionAttrTypeId_;
  std::vector<bool>  regionAttrPartitionsFlag_;
  std::vector<uint8_t>  regionAttrPartitionIndex_;
  std::vector<uint8_t>  regionAttrPartitionsMinus1_;
};
// 7.3.4.7	V3C parameter set MIV extension syntax
class VpsMivExtension {
 public:
  VpsMivExtension()  :
     depthLowQualityFlag_(false),
     geometryScaleEnableFlag_(false),
     numGroupsMinus1_(0),
     maxEntitiesMinus1_(0),
     embeddedOccupancyFlag_(true),
     occcupancyScaleEnabledFlag_ (false) {
      packedVideoPresentFlag_.clear();
      packingInformation_.clear();
   }
  ~VpsMivExtension() {}
  VpsMivExtension& operator=( const VpsMivExtension& ) = default;
    
  bool     getDepthLowQualityFlag() { return depthLowQualityFlag_; }
  void     setDepthLowQualityFlag(bool value) { depthLowQualityFlag_ = value; }
  bool     getGeometryScaleEnableFlag() { return geometryScaleEnableFlag_; }
  void     setGeometryScaleEnableFlag(bool value) { geometryScaleEnableFlag_ = value; }
  uint32_t getNumGroupsMinus1() { return numGroupsMinus1_; }
  void     setNumGroupsMinus1(uint32_t value) { numGroupsMinus1_ = value; }
  uint32_t getMaxEntitiesMinus1() { return maxEntitiesMinus1_; }
  void     setMaxEntitiesMinus1(uint32_t value) { maxEntitiesMinus1_= value; }

  bool    getEmbeddedOccupancyFlag() { return embeddedOccupancyFlag_; }
  void    setEmbeddedOccupancyFlag( bool value ) { embeddedOccupancyFlag_ = value; }
  bool    getOcccupancyScaleEnabledFlag() { return occcupancyScaleEnabledFlag_; }
  void    setOcccupancyScaleEnabledFlag( bool value ) { occcupancyScaleEnabledFlag_ = value; }
  
  void allocateFlags( size_t size ) {
    packedVideoPresentFlag_.resize( size );
    packingInformation_.resize( size );
  }
  bool getPackedVideoPresentFlag( size_t index ) { return packedVideoPresentFlag_[index]; }
  void setPackedVideoPresentFlag( size_t index, bool value ) { packedVideoPresentFlag_[index] = value; }

  PackingInformation& getPackingInformation( size_t index ) { return packingInformation_[index]; }

 private:
  bool          depthLowQualityFlag_; 
  bool          geometryScaleEnableFlag_; 
  uint32_t      numGroupsMinus1_; 
  uint32_t      maxEntitiesMinus1_; 
  bool          embeddedOccupancyFlag_;
  bool          occcupancyScaleEnabledFlag_;
  std::vector<bool> packedVideoPresentFlag_;
  std::vector<PackingInformation> packingInformation_;
};

// 7.3.6.1.3	Atlas sequence parameters MIV extension syntax
class AspsMivExtension {
 public:
  AspsMivExtension() :
     groupId_(0),
     auxiliaryAtlasFlag_(false),
     depthOccMapThresholdFlag_(false),
     occupancyScalePresentFlag_(false),
     occupancyScaleXMinus1_(0),
     occupancyScaleYMinus1_(0),
     geometryScaleFactorXMinus1_(0),
     geometryScaleFactorYMinus1_(0),
     patchConstantDepthFlag_(false) {}
  ~AspsMivExtension() {}
  AspsMivExtension& operator=( const AspsMivExtension& ) = default;

  uint32_t getGroupsId() { return groupId_; }
  void     setGroupsId(uint32_t value) { groupId_ = value; }
  bool     getAuxiliaryAtlasFlag() { return auxiliaryAtlasFlag_; }
  void     setAuxiliaryAtlasFlag(bool value) { auxiliaryAtlasFlag_ = value; }
  bool     getDepthOccMapThresholdFlag() { return depthOccMapThresholdFlag_; }
  void     setDepthOccMapThresholdFlag(bool value) { depthOccMapThresholdFlag_ = value; }
  uint32_t getGeometryScaleFactorYMinus1() { return geometryScaleFactorYMinus1_; }
  void     setGeometryScaleFactorYMinus1(uint32_t value) { geometryScaleFactorYMinus1_ = value; }
  uint32_t getGeometryScaleFactorXMinus1() { return geometryScaleFactorXMinus1_; }
  void     setGeometryScaleFactorXMinus1(uint32_t value) { geometryScaleFactorXMinus1_ = value; }
  bool     getOccupancyScalePresentFlag() { return occupancyScalePresentFlag_; }
  void     setOccupancyScalePresentFlag(bool value) { occupancyScalePresentFlag_ = value; }
  uint32_t getOccupancyScaleXMinus1() { return occupancyScaleXMinus1_; }
  void     setOccupancyScaleXMinus1(uint32_t value) { occupancyScaleXMinus1_ = value; }
  uint32_t getOccupancyScaleYMinus1() { return occupancyScaleYMinus1_; }
  void     setOccupancyScaleYMinus1( uint32_t value ) { occupancyScaleYMinus1_ = value; }
  bool     getPatchConstantDepthFlag() { return patchConstantDepthFlag_; }
  void     setPatchConstantDepthFlag( bool value ) { patchConstantDepthFlag_ = value; }

 private:
  uint32_t      groupId_; 
  bool          auxiliaryAtlasFlag_;
  bool          depthOccMapThresholdFlag_;
  bool          occupancyScalePresentFlag_;
  uint32_t      occupancyScaleXMinus1_;
  uint32_t      occupancyScaleYMinus1_;
  uint32_t      geometryScaleFactorXMinus1_;
  uint32_t      geometryScaleFactorYMinus1_;
  bool          patchConstantDepthFlag_;
};

// 7.3.6.2	Atlas frame parameter set RBSP syntax --> Currently not being used
class AfpsMivExtension {
 public:
  AfpsMivExtension() {}
  ~AfpsMivExtension() {}
  AfpsMivExtension& operator=( const AfpsMivExtension& ) = default;

 private:
};

// 7.3.6.3.2	Atlas adaptation parameter MIV extension syntax 
class AapsMivExtension {
 public:
  AapsMivExtension() :
      omafV1CompatibleFlag_( false ),
      vuiParamsPresentFlag_( false )
  {}
  ~AapsMivExtension() {}
  AapsMivExtension& operator=( const AapsMivExtension& ) = default;

  bool     getOmafV1CompatibleFlag() { return omafV1CompatibleFlag_; }
  void     setOmafV1CompatibleFlag(bool value) { omafV1CompatibleFlag_ = value; }
  bool     getVuiParamsPresentFlag() { return vuiParamsPresentFlag_; }
  void     setVuiParamsPresentFlag(bool value) { vuiParamsPresentFlag_ = value; }
  VUIParameters&     getVuiParameters() { return vuiParameters_; }

 private:
  bool                           omafV1CompatibleFlag_; 
  bool                           vuiParamsPresentFlag_; 
  VUIParameters                  vuiParameters_; 
};

// 7.3.6.13.1	General common atlas frame RBSP syntax
class CommonAtlasFrameRbsp {
 public:
  CommonAtlasFrameRbsp() : 
    aapsID_( 0 ), 
    frmOrderCntLsb_( 0 ), 
    viewParametersListUpdateMode_( 0 ),
      extensionFlag_( false ),
      extension8Bits_(0)
  {
    extensionDataFlag_.clear();
  }
  ~CommonAtlasFrameRbsp() { 
      extensionDataFlag_.clear(); 
  }
  CommonAtlasFrameRbsp& operator=( const CommonAtlasFrameRbsp& ) = default;

  size_t getAtlasAdaptationParameterSetID() { return aapsID_; }
  void   setAtlasAdaptationParameterSetID( size_t value ) { aapsID_ = value; }
  size_t getFrmOrderCntLsb() { return frmOrderCntLsb_; }
  void   setFrmOrderCntLsb( size_t value ) { frmOrderCntLsb_ = value; }

  uint8_t getViewParametersListUpdateMode() { return viewParametersListUpdateMode_; }
  void    setViewParametersListUpdateMode( uint8_t value ) { viewParametersListUpdateMode_ = value; }
  ViewParametersList& getViewParametersList() { return viewParametersList_; }
  ViewParametersUpdateExtrinsics& getViewParametersUpdateExtrinsics() { return viewParametersUpdateExtrinsics_; }
  ViewParametersUpdateIntrinsics& getViewParametersUpdateIntrinsics() { return viewParametersUpdateIntrinsics_; }
  ViewParametersUpdateDepthQuantization& getViewParametersUpdateDepthQuantization() { return viewParametersUpdateDepthQuantization_; }

  bool getExtensionFlag() { return extensionFlag_; }
  void    setExtensionFlag( bool value ) { extensionFlag_ = value; }
  uint8_t getExtension8Bits() { return extension8Bits_; }
  void    setExtension8Bits( uint8_t value ) { extension8Bits_ = value; }
  std::vector<bool>&    getExtensionDataFlagList() { return extensionDataFlag_; }

 private:
  size_t aapsID_;
  size_t frmOrderCntLsb_;
  uint8_t viewParametersListUpdateMode_;
  ViewParametersList             viewParametersList_;
  ViewParametersUpdateExtrinsics viewParametersUpdateExtrinsics_;
  ViewParametersUpdateIntrinsics viewParametersUpdateIntrinsics_;
  ViewParametersUpdateDepthQuantization viewParametersUpdateDepthQuantization_;
  bool                                  extensionFlag_;
  uint8_t                               extension8Bits_;
  std::vector<bool>                  extensionDataFlag_;

};

// 7.3.7.4	Patch data unit MIV extension syntax  
class PduMivExtension {
 public:
  PduMivExtension() :
    entityId_ (0),
    depthOccThreshold_(0)
  {}
  ~PduMivExtension() {}
  PduMivExtension& operator=( const PduMivExtension& ) = default;

  size_t getEntityId() { return entityId_; }
  size_t getDepthOccThreshold() { return depthOccThreshold_; }
  void   setEntityId( size_t value ) { entityId_ = value; }
  void   setDepthOccThreshold( size_t value ) { depthOccThreshold_= value; }

 private:
  size_t                         entityId_;
  size_t                         depthOccThreshold_;
};

class MpduMivExtension {
 public:
  MpduMivExtension()  {}
  ~MpduMivExtension() {}
  MpduMivExtension& operator=( const MpduMivExtension& ) = default;
 private:
};

class IpduMivExtension {
 public:
  IpduMivExtension()  {}
  ~IpduMivExtension() {}
  IpduMivExtension& operator=( const IpduMivExtension& ) = default;
 private:
};

};  // namespace pcc

#endif  //~PCC_BITSTREAM_MIVEXTENSION_H