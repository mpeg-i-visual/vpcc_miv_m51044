/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2017, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef PCC_BITSTREAM_CAMERAEXTRINSICS_H
#define PCC_BITSTREAM_CAMERAEXTRINSICS_H

#include "PCCBitstreamCommon.h"

namespace pcc {
// Specified in ISO/IEC 23090-12
// 7.3.6.3.9	Camera extrinsics syntax
class CameraExtrinsics {
 public:
  CameraExtrinsics() : 
		viewPosX_ (0.0),
	  viewPosY_ (0.0),
		viewPosZ_ (0.0),
    viewScaleX_(1),
    viewScaleY_(1),
    viewScaleZ_(1),
	  viewQuaternionX_ (0.0),
		viewQuaternionY_ (0.0),
	  viewQuaternionZ_ (0.0)	{ 
	}
  ~CameraExtrinsics() { 
	}

	CameraExtrinsics& operator=( const CameraExtrinsics& ) = default;
  
	float getViewPosX()              { return viewPosX_; }
  void  setViewPosX( float value ) { viewPosX_ = value; }
	float getViewPosY()              { return viewPosY_; }
  void  setViewPosY( float value ) { viewPosY_ = value; }
	float getViewPosZ()              { return viewPosZ_; }
  void  setViewPosZ( float value ) { viewPosZ_ = value; }
  void  set( float valueX, float valueY, float valueZ ) { 
    viewPosX_ = valueX; 
    viewPosY_ = valueY; 
    viewPosZ_ = valueZ; 
  }
  
  
	float getViewScaleX()              { return viewScaleX_; }
  void  setViewScaleX( float value ) { viewScaleX_ = value; }
	float getViewScaleY()              { return viewScaleY_; }
  void  setViewScaleY( float value ) { viewScaleY_ = value; }
	float getViewScaleZ()              { return viewScaleZ_; }
  void  setViewScaleZ( float value ) { viewScaleZ_ = value; }

	float getViewQuaternionX()              { return viewQuaternionX_; }
  void  setViewQuaternionX( float value ) { viewQuaternionX_ = value; }
	float getViewQuaternionY()              { return viewQuaternionY_; }
  void  setViewQuaternionY( float value ) { viewQuaternionY_ = value; }
	float getViewQuaternionZ()              { return viewQuaternionZ_; }
  void  setViewQuaternionZ( float value ) { viewQuaternionZ_ = value; }
  void  setViewQuaternion( float valueX, float valueY, float valueZ ) {  
   viewQuaternionX_ = valueX;
   viewQuaternionY_ = valueY;
   viewQuaternionZ_ = valueZ;
   viewQuaternionW_ = sqrtf((float)1.0 -
     (viewQuaternionX_*viewQuaternionX_ +
       viewQuaternionY_ * viewQuaternionY_ +
       viewQuaternionZ_ * viewQuaternionZ_));
  }

 private:
	float                           viewPosX_;
	float                           viewPosY_;
	float                           viewPosZ_;
	float                           viewScaleX_;
	float                           viewScaleY_;
	float                           viewScaleZ_;
	float                           viewQuaternionX_;
	float                           viewQuaternionY_;
	float                           viewQuaternionZ_;
	float                           viewQuaternionW_;
};
};  // namespace pcc

#endif //~PCC_BITSTREAM_CAMERAEXTRINSICS_H
