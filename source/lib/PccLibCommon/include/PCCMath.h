/* The copyright in this software is being made available under the BSD
 * License, included below. This software may be subject to other third party
 * and contributor rights, including patent rights, and no such rights are
 * granted under this license.
 *
 * Copyright (c) 2010-2017, ISO/IEC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *  * Neither the name of the ISO/IEC nor the names of its contributors may
 *    be used to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PCCMath_h
#define PCCMath_h

#define _USE_MATH_DEFINES
#include <cmath>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#include "PCCCommon.h"

namespace pcc {

typedef int16_t PCCType;

/// Vector dim 2
template <typename T>
class PCCVector2 {
public:
  T& operator[](size_t i) {
    assert(i < 2);
    return data_[i];
  }
  const T& operator[](size_t i) const {
    assert(i < 2);
    return data_[i];
  }
  size_t   getElementCount() const { return 2; }
  T&       u() { return data_[0]; }
  T&       v() { return data_[1]; }
  const T& u() const { return data_[0]; }
  const T& v() const { return data_[1]; }
  T&       x() { return data_[0]; }
  T&       y() { return data_[1]; }
  const T& x() const { return data_[0]; }
  const T& y() const { return data_[1]; }
  void     normalize() {
    const T norm2 = data_[0] * data_[0] + data_[1] * data_[1];
    if (norm2 != 0.0) {
      T invNorm = static_cast<T>(1.0 / sqrt(norm2));
      (*this) *= invNorm;
    }
  }
  T           getNorm() const { return static_cast<T>(sqrt(getNorm2())); }
  T           getNorm2() const { return data_[0] * data_[0] + data_[1] * data_[1]; }
  PCCVector2& operator=(const PCCVector2& rhs) {
    memcpy(data_, rhs.data_, sizeof(data_));
    return *this;
  }
  PCCVector2& operator+=(const PCCVector2& rhs) {
    data_[0] += rhs.data_[0];
    data_[1] += rhs.data_[1];
    return *this;
  }
  PCCVector2& operator-=(const PCCVector2& rhs) {
    data_[0] -= rhs.data_[0];
    data_[1] -= rhs.data_[1];
    return *this;
  }
  PCCVector2& operator-=(const T a) {
    data_[0] -= a;
    data_[1] -= a;
    return *this;
  }
  PCCVector2& operator+=(const T a) {
    data_[0] += a;
    data_[1] += a;
    return *this;
  }
  PCCVector2& operator/=(const T a) {
    assert(a != 0);
    data_[0] /= a;
    data_[1] /= a;
    return *this;
  }
  PCCVector2& operator*=(const T a) {
    data_[0] *= a;
    data_[1] *= a;
    return *this;
  }
  PCCVector2& operator=(const T a) {
    data_[0] = a;
    data_[1] = a;
    return *this;
  }
  PCCVector2& operator=(const T* const rhs) {
    data_[0] = rhs[0];
    data_[1] = rhs[1];
    return *this;
  }

  // T operator*(const PCCVector3 &rhs) const {
  //   return (data_[0] * rhs.data_[0] + data_[1] * rhs.data_[1] + data_[2] * rhs.data_[2]);
  // }
  PCCVector2        operator-() const { return PCCVector2<T>(-data_[0], -data_[1]); }
  friend PCCVector2 operator+(const PCCVector2& lhs, const PCCVector2& rhs) {
    return PCCVector2<T>(lhs.data_[0] + rhs.data_[0], lhs.data_[1] + rhs.data_[1]);
  }
  friend PCCVector2 operator+(const T lhs, const PCCVector2& rhs) {
    return PCCVector2<T>(lhs + rhs.data_[0], lhs + rhs.data_[1]);
  }
  friend PCCVector2 operator+(const PCCVector2& lhs, const T rhs) {
    return PCCVector2<T>(lhs.data_[0] + rhs, lhs.data_[1] + rhs);
  }
  friend PCCVector2 operator-(const PCCVector2& lhs, const PCCVector2& rhs) {
    return PCCVector2<T>(lhs.data_[0] - rhs.data_[0], lhs.data_[1] - rhs.data_[1]);
  }
  friend PCCVector2 operator-(const T lhs, const PCCVector2& rhs) {
    return PCCVector2<T>(lhs - rhs.data_[0], lhs - rhs.data_[1]);
  }
  friend PCCVector2 operator-(const PCCVector2& lhs, const T rhs) {
    return PCCVector2<T>(lhs.data_[0] - rhs, lhs.data_[1] - rhs);
  }
  friend PCCVector2 operator*(const T lhs, const PCCVector2& rhs) {
    return PCCVector2<T>(lhs * rhs.data_[0], lhs * rhs.data_[1]);
  }
  friend PCCVector2 operator*(const PCCVector2& lhs, const T rhs) {
    return PCCVector2<T>(lhs.data_[0] * rhs, lhs.data_[1] * rhs);
  }
  friend PCCVector2 operator/(const PCCVector2& lhs, const T rhs) {
    assert(rhs != 0);
    return PCCVector2<T>(lhs.data_[0] / rhs, lhs.data_[1] / rhs);
  }
  bool operator<(const PCCVector2& rhs) const {
    if (data_[0] == rhs.data_[0]) {
      return (data_[1] < rhs.data_[1]);
    }
    return (data_[0] < rhs.data_[0]);
  }
  bool operator>(const PCCVector2& rhs) const {
    if (data_[0] == rhs.data_[0]) {
      if (data_[1] == rhs.data_[1]) { return (data_[2] > rhs.data_[2]); }
      return (data_[1] > rhs.data_[1]);
    }
    return (data_[0] > rhs.data_[0]);
  }
  bool operator==(const PCCVector2& rhs) const {
    return (data_[0] == rhs.data_[0] && data_[1] == rhs.data_[1] );
  }
  bool operator!=(const PCCVector2& rhs) const {
    return (data_[0] != rhs.data_[0] || data_[1] != rhs.data_[1]);
  }
  friend std::ostream& operator<<(std::ostream& os, const PCCVector2& vec) {
    os << vec[0] << " " << vec[1] << std::endl;
    return os;
  }
  friend std::istream& operator>>(std::istream& is, PCCVector2& vec) {
    is >> vec[0] >> vec[1] ;
    return is;
  }
  PCCVector2(const T a) { data_[0] = data_[1] = a; }
  PCCVector2(const T x, const T y) {
    data_[0] = x;
    data_[1] = y;
  }
  PCCVector2(const PCCVector2& vec) {
    data_[0] = vec.data_[0];
    data_[1] = vec.data_[1];
  }
  PCCVector2(const T* vec) { memcpy(data_, vec, sizeof(data_)); }
  PCCVector2() = default;
  ~PCCVector2(void) = default;

private:
  T data_[2];
};

/// Vector dim 3
template <typename T>
class PCCVector3 {
 public:
  T& operator[]( size_t i ) {
    assert( i < 3 );
    return data_[i];
  }
  const T& operator[]( size_t i ) const {
    assert( i < 3 );
    return data_[i];
  }
  size_t   getElementCount() const { return 3; }
  T&       r() { return data_[0]; }
  T&       g() { return data_[1]; }
  T&       b() { return data_[2]; }
  const T& r() const { return data_[0]; }
  const T& g() const { return data_[1]; }
  const T& b() const { return data_[2]; }
  T&       x() { return data_[0]; }
  T&       y() { return data_[1]; }
  T&       z() { return data_[2]; }
  const T& x() const { return data_[0]; }
  const T& y() const { return data_[1]; }
  const T& z() const { return data_[2]; }
  void     normalize() {
    const T norm2 = data_[0] * data_[0] + data_[1] * data_[1] + data_[2] * data_[2];
    if ( norm2 != 0.0 ) {
      T invNorm = static_cast<T>( 1.0 / sqrt( norm2 ) );
      ( *this ) *= invNorm;
    }
  }
  T           getNorm() const { return static_cast<T>( sqrt( getNorm2() ) ); }
  T           getNorm2() const { return data_[0] * data_[0] + data_[1] * data_[1] + data_[2] * data_[2]; }
  PCCVector3& operator=( const PCCVector3& rhs ) {
    memcpy( data_, rhs.data_, sizeof( data_ ) );
    return *this;
  }
  PCCVector3& operator+=( const PCCVector3& rhs ) {
    data_[0] += rhs.data_[0];
    data_[1] += rhs.data_[1];
    data_[2] += rhs.data_[2];
    return *this;
  }
  PCCVector3& operator-=( const PCCVector3& rhs ) {
    data_[0] -= rhs.data_[0];
    data_[1] -= rhs.data_[1];
    data_[2] -= rhs.data_[2];
    return *this;
  }
  PCCVector3& operator-=( const T a ) {
    data_[0] -= a;
    data_[1] -= a;
    data_[2] -= a;
    return *this;
  }
  PCCVector3& operator+=( const T a ) {
    data_[0] += a;
    data_[1] += a;
    data_[2] += a;
    return *this;
  }
  PCCVector3& operator/=( const T a ) {
    assert( a != 0 );
    data_[0] /= a;
    data_[1] /= a;
    data_[2] /= a;
    return *this;
  }
  PCCVector3& operator*=( const T a ) {
    data_[0] *= a;
    data_[1] *= a;
    data_[2] *= a;
    return *this;
  }
  PCCVector3& operator=( const T a ) {
    data_[0] = a;
    data_[1] = a;
    data_[2] = a;
    return *this;
  }
  PCCVector3& operator=( const T* const rhs ) {
    data_[0] = rhs[0];
    data_[1] = rhs[1];
    data_[2] = rhs[2];
    return *this;
  }

  // T operator*(const PCCVector3 &rhs) const {
  //   return (data_[0] * rhs.data_[0] + data_[1] * rhs.data_[1] + data_[2] *
  // rhs.data_[2]);
  // }
  PCCVector3 operator^( const PCCVector3& rhs ) const {
    return PCCVector3<T>( data_[1] * rhs.data_[2] - data_[2] * rhs.data_[1],
                          data_[2] * rhs.data_[0] - data_[0] * rhs.data_[2],
                          data_[0] * rhs.data_[1] - data_[1] * rhs.data_[0] );
  }
  PCCVector3        operator-() const { return PCCVector3<T>( -data_[0], -data_[1], -data_[2] ); }
  friend PCCVector3 operator+( const PCCVector3& lhs, const PCCVector3& rhs ) {
    return PCCVector3<T>( lhs.data_[0] + rhs.data_[0], lhs.data_[1] + rhs.data_[1], lhs.data_[2] + rhs.data_[2] );
  }
  friend PCCVector3 operator+( const T lhs, const PCCVector3& rhs ) {
    return PCCVector3<T>( lhs + rhs.data_[0], lhs + rhs.data_[1], lhs + rhs.data_[2] );
  }
  friend PCCVector3 operator+( const PCCVector3& lhs, const T rhs ) {
    return PCCVector3<T>( lhs.data_[0] + rhs, lhs.data_[1] + rhs, lhs.data_[2] + rhs );
  }
  friend PCCVector3 operator-( const PCCVector3& lhs, const PCCVector3& rhs ) {
    return PCCVector3<T>( lhs.data_[0] - rhs.data_[0], lhs.data_[1] - rhs.data_[1], lhs.data_[2] - rhs.data_[2] );
  }
  friend PCCVector3 operator-( const T lhs, const PCCVector3& rhs ) {
    return PCCVector3<T>( lhs - rhs.data_[0], lhs - rhs.data_[1], lhs - rhs.data_[2] );
  }
  friend PCCVector3 operator-( const PCCVector3& lhs, const T rhs ) {
    return PCCVector3<T>( lhs.data_[0] - rhs, lhs.data_[1] - rhs, lhs.data_[2] - rhs );
  }
  friend PCCVector3 operator*( const T lhs, const PCCVector3& rhs ) {
    return PCCVector3<T>( lhs * rhs.data_[0], lhs * rhs.data_[1], lhs * rhs.data_[2] );
  }
  friend PCCVector3 operator*( const PCCVector3& lhs, const T rhs ) {
    return PCCVector3<T>( lhs.data_[0] * rhs, lhs.data_[1] * rhs, lhs.data_[2] * rhs );
  }
  friend PCCVector3 operator/( const PCCVector3& lhs, const T rhs ) {
    assert( rhs != 0 );
    return PCCVector3<T>( lhs.data_[0] / rhs, lhs.data_[1] / rhs, lhs.data_[2] / rhs );
  }
  bool operator<( const PCCVector3& rhs ) const {
    if ( data_[0] == rhs.data_[0] ) {
      if ( data_[1] == rhs.data_[1] ) { return ( data_[2] < rhs.data_[2] ); }
      return ( data_[1] < rhs.data_[1] );
    }
    return ( data_[0] < rhs.data_[0] );
  }
  bool operator>( const PCCVector3& rhs ) const {
    if ( data_[0] == rhs.data_[0] ) {
      if ( data_[1] == rhs.data_[1] ) { return ( data_[2] > rhs.data_[2] ); }
      return ( data_[1] > rhs.data_[1] );
    }
    return ( data_[0] > rhs.data_[0] );
  }
  bool operator==( const PCCVector3& rhs ) const {
    return ( data_[0] == rhs.data_[0] && data_[1] == rhs.data_[1] && data_[2] == rhs.data_[2] );
  }
  bool operator!=( const PCCVector3& rhs ) const {
    return ( data_[0] != rhs.data_[0] || data_[1] != rhs.data_[1] || data_[2] != rhs.data_[2] );
  }
  friend std::ostream& operator<<( std::ostream& os, const PCCVector3& vec ) {
    os << vec[0] << " " << vec[1] << " " << vec[2] << std::endl;
    return os;
  }
  friend std::istream& operator>>( std::istream& is, PCCVector3& vec ) {
    is >> vec[0] >> vec[1] >> vec[2];
    return is;
  }
  PCCVector3( const T a ) { data_[0] = data_[1] = data_[2] = a; }
  PCCVector3( const T x, const T y, const T z ) {
    data_[0] = x;
    data_[1] = y;
    data_[2] = z;
  }
  PCCVector3( const PCCVector3& vec ) {
    data_[0] = vec.data_[0];
    data_[1] = vec.data_[1];
    data_[2] = vec.data_[2];
  }
  PCCVector3( const T* vec ) { memcpy( data_, vec, sizeof( data_ ) ); }
  PCCVector3()        = default;
  ~PCCVector3( void ) = default;

 private:
  T data_[3];
};

/// Vector dim 4
template <typename T>
class PCCVector4 {
public:
  T& operator[](size_t i) {
    assert(i < 4);
    return data_[i];
  }
  const T& operator[](size_t i) const {
    assert(i < 4);
    return data_[i];
  }
  size_t   getElementCount() const { return 4; }
  T&       x() { return data_[0]; }
  T&       y() { return data_[1]; }
  T&       z() { return data_[2]; }
  T&       w() { return data_[3]; }
  const T& x() const { return data_[0]; }
  const T& y() const { return data_[1]; }
  const T& z() const { return data_[2]; }
  const T& w() const { return data_[3]; }
  void     normalize() {
    const T norm2 = data_[0] * data_[0] + data_[1] * data_[1] + data_[2] * data_[2] + data_[3] * data_[3];
    if (norm2 != 0.0) {
      T invNorm = static_cast<T>(1.0 / sqrt(norm2));
      (*this) *= invNorm;
    }
  }
  T           getNorm() const { return static_cast<T>(sqrt(getNorm2())); }
  T           getNorm2() const { return data_[0] * data_[0] + data_[1] * data_[1] + data_[2] * data_[2] + data_[3] * data_[3]; }
  PCCVector4& operator=(const PCCVector4& rhs) {
    memcpy(data_, rhs.data_, sizeof(data_));
    return *this;
  }
  PCCVector4& operator+=(const PCCVector4& rhs) {
    data_[0] += rhs.data_[0];
    data_[1] += rhs.data_[1];
    data_[2] += rhs.data_[2];
    data_[3] += rhs.data_[3];
    return *this;
  }
  PCCVector4& operator-=(const PCCVector4& rhs) {
    data_[0] -= rhs.data_[0];
    data_[1] -= rhs.data_[1];
    data_[2] -= rhs.data_[2];
    data_[3] -= rhs.data_[3];
    return *this;
  }
  PCCVector4& operator-=(const T a) {
    data_[0] -= a;
    data_[1] -= a;
    data_[2] -= a;
    data_[3] -= a;
    return *this;
  }
  PCCVector4& operator+=(const T a) {
    data_[0] += a;
    data_[1] += a;
    data_[2] += a;
    data_[3] += a;
    return *this;
  }
  PCCVector4& operator/=(const T a) {
    assert(a != 0);
    data_[0] /= a;
    data_[1] /= a;
    data_[2] /= a;
    data_[3] /= a;
    return *this;
  }
  PCCVector4& operator*=(const T a) {
    data_[0] *= a;
    data_[1] *= a;
    data_[2] *= a;
    data_[3] *= a;
    return *this;
  }
  PCCVector4& operator=(const T a) {
    data_[0] = a;
    data_[1] = a;
    data_[2] = a;
    data_[3] = a;
    return *this;
  }
  PCCVector4& operator=(const T* const rhs) {
    data_[0] = rhs[0];
    data_[1] = rhs[1];
    data_[2] = rhs[2];
    data_[3] = rhs[3];
    return *this;
  }

  PCCVector4        operator-() const { return PCCVector4<T>(-data_[0], -data_[1], -data_[2], -data_[3]); }
  friend PCCVector4 operator+(const PCCVector4& lhs, const PCCVector4& rhs) {
    return PCCVector4<T>(lhs.data_[0] + rhs.data_[0], lhs.data_[1] + rhs.data_[1], lhs.data_[2] + rhs.data_[2], lhs.data_[3] + rhs.data_[3]);
  }
  friend PCCVector4 operator+(const T lhs, const PCCVector4& rhs) {
    return PCCVector4<T>(lhs + rhs.data_[0], lhs + rhs.data_[1], lhs + rhs.data_[2], lhs + rhs.data_[3]);
  }
  friend PCCVector4 operator+(const PCCVector4& lhs, const T rhs) {
    return PCCVector4<T>(lhs.data_[0] + rhs, lhs.data_[1] + rhs, lhs.data_[2] + rhs, lhs.data_[3] + rhs);
  }
  friend PCCVector4 operator-(const PCCVector4& lhs, const PCCVector4& rhs) {
    return PCCVector4<T>(lhs.data_[0] - rhs.data_[0], lhs.data_[1] - rhs.data_[1], lhs.data_[2] - rhs.data_[2], lhs.data_[3] - rhs.data_[3]);
  }
  friend PCCVector4 operator-(const T lhs, const PCCVector4& rhs) {
    return PCCVector4<T>(lhs - rhs.data_[0], lhs - rhs.data_[1], lhs - rhs.data_[2], lhs - rhs.data_[3]);
  }
  friend PCCVector4 operator-(const PCCVector4& lhs, const T rhs) {
    return PCCVector4<T>(lhs.data_[0] - rhs, lhs.data_[1] - rhs, lhs.data_[2] - rhs, lhs.data_[3] - rhs);
  }
  friend PCCVector4 operator*(const T lhs, const PCCVector4& rhs) {
    return PCCVector4<T>(lhs * rhs.data_[0], lhs * rhs.data_[1], lhs * rhs.data_[2], lhs * rhs.data_[3]);
  }
  friend PCCVector4 operator*(const PCCVector4& lhs, const T rhs) {
    return PCCVector4<T>(lhs.data_[0] * rhs, lhs.data_[1] * rhs, lhs.data_[2] * rhs, lhs.data_[3] * rhs);
  }
  friend PCCVector4 operator/(const PCCVector4& lhs, const T rhs) {
    assert(rhs != 0);
    return PCCVector4<T>(lhs.data_[0] / rhs, lhs.data_[1] / rhs, lhs.data_[2] / rhs, lhs.data_[3] / rhs);
  }
  bool operator<(const PCCVector4& rhs) const {
    if (data_[0] == rhs.data_[0]) {
      if (data_[1] == rhs.data_[1]) { 
        if (data_[2] == rhs.data_[2]) {
          return (data_[3] < rhs.data_[3]);
        }
        return (data_[2] < rhs.data_[2]);
      }
      return (data_[1] < rhs.data_[1]);
    }
    return (data_[0] < rhs.data_[0]);
  }
  bool operator>(const PCCVector4& rhs) const {
    if (data_[0] == rhs.data_[0]) {
      if (data_[1] == rhs.data_[1]) { 
        if (data_[2] == rhs.data_[2]) {
          return (data_[3] > rhs.data_[3]);
        }
        return (data_[2] > rhs.data_[2]);
      }
      return (data_[1] > rhs.data_[1]);
    }
    return (data_[0] > rhs.data_[0]);
  }
  bool operator==(const PCCVector4& rhs) const {
    return (data_[0] == rhs.data_[0] && data_[1] == rhs.data_[1] && data_[2] == rhs.data_[2] && data_[3] == rhs.data_[3]);
  }
  bool operator!=(const PCCVector4& rhs) const {
    return (data_[0] != rhs.data_[0] || data_[1] != rhs.data_[1] || data_[2] != rhs.data_[2] || data_[3] != rhs.data_[3]);
  }
  friend std::ostream& operator<<(std::ostream& os, const PCCVector4& vec) {
    os << vec[0] << " " << vec[1] << " " << vec[2] << vec[3] << std::endl;
    return os;
  }
  friend std::istream& operator>>(std::istream& is, PCCVector4& vec) {
    is >> vec[0] >> vec[1] >> vec[2] >> vec[3];
    return is;
  }
  PCCVector4(const T a) { data_[0] = data_[1] = data_[2] = data_[3] = a; }
  PCCVector4(const T x, const T y, const T z, const T w = 1) {
    data_[0] = x;
    data_[1] = y;
    data_[2] = z;
    data_[2] = w;
  }
  PCCVector4(const PCCVector4& vec) {
    data_[0] = vec.data_[0];
    data_[1] = vec.data_[1];
    data_[2] = vec.data_[2];
    data_[3] = vec.data_[3];
  }
  PCCVector4(const PCCVector3<T>& vec) {
    data_[0] = vec.data_[0];
    data_[1] = vec.data_[1];
    data_[2] = vec.data_[2];
    data_[3] = 1;
  }
  PCCVector4(const T* vec) { memcpy(data_, vec, sizeof(data_)); }
  PCCVector4() = default;
  ~PCCVector4(void) = default;

private:
  T data_[4];
};

template <typename T>
struct PCCBox3 {
  PCCVector3<T> min_;
  PCCVector3<T> max_;
  bool          contains( const PCCVector3<T> point ) const {
    return !( point.x() < min_.x() || point.x() > max_.x() || point.y() < min_.y() || point.y() > max_.y() ||
              point.z() < min_.z() || point.z() > max_.z() );
  }

  PCCBox3 merge( const PCCBox3& box ) {
    min_.x() = std::min( min_.x(), box.min_.x() );
    min_.y() = std::min( min_.y(), box.min_.y() );
    min_.z() = std::min( min_.z(), box.min_.z() );
    max_.x() = std::max( max_.x(), box.max_.x() );
    max_.y() = std::max( max_.y(), box.max_.y() );
    max_.z() = std::max( max_.z(), box.max_.z() );
    return box;
  }
  inline void add( const PCCVector3<T>& point ) {
    if ( min_[0] > point[0] ) { min_[0] = point[0]; }
    if ( min_[1] > point[1] ) { min_[1] = point[1]; }
    if ( min_[2] > point[2] ) { min_[2] = point[2]; }
    if ( max_[0] < point[0] ) { max_[0] = point[0]; }
    if ( max_[1] < point[1] ) { max_[1] = point[1]; }
    if ( max_[2] < point[2] ) { max_[2] = point[2]; }
  }
  bool intersects( const PCCBox3& box ) {
    return max_.x() >= box.min_.x() && min_.x() <= box.max_.x() && max_.y() >= box.min_.y() &&
           min_.y() <= box.max_.y() && max_.z() >= box.min_.z() && min_.z() <= box.max_.z();
  }

  bool fullyContains( const PCCBox3& box ) {
    return max_.x() >= box.max_.x() && min_.x() <= box.min_.x() && max_.y() >= box.max_.y() &&
           min_.y() <= box.min_.y() && max_.z() >= box.max_.z() && min_.z() <= box.min_.z();
  }

  bool fullyContains( const PCCVector3<int16_t> point ) const {
    return max_.x() >= point.x() && min_.x() <= point.x() && max_.y() >= point.y() && min_.y() <= point.y() &&
           max_.z() >= point.z() && min_.z() <= point.z();
  }

  friend std::ostream& operator<<( std::ostream& os, const PCCBox3& box ) {
    os << box.min_[0] << " " << box.min_[1] << " " << box.min_[2] << " " << box.max_[0] << " " << box.max_[1] << " "
       << box.max_[2] << std::endl;
    return os;
  }
  friend std::istream& operator>>( std::istream& is, PCCBox3& box ) {
    is >> box.min_[0] >> box.min_[1] >> box.min_[2] >> box.max_[0] >> box.max_[1] >> box.max_[2];
    return is;
  }
};

//!    2x2 Matrix
template <typename T>
class PCCMatrix2 {
public:
  T* operator[](const size_t rowIndex) {
    assert(rowIndex < 2);
    return data_[rowIndex];
  }
  const T* operator[](const size_t rowIndex) const {
    assert(rowIndex < 2);
    return data_[rowIndex];
  }
  size_t      getColumnCount() const { return 2; }
  size_t      getRowCount() const { return 2; }
  PCCMatrix2& operator=(const PCCMatrix2& rhs) {
    memcpy(data_, rhs.data_, sizeof(data_));
    return *this;
  }
  void operator+=(const PCCMatrix2& rhs) {
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { this->data_[i][j] += rhs.data_[i][j]; }
    }
  }
  void operator-=(const PCCMatrix2& rhs) {
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { this->data_[i][j] -= rhs.data_[i][j]; }
    }
  }
  void operator-=(const T a) {
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { this->data_[i][j] -= a; }
    }
  }
  void operator+=(const T a) {
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { this->data_[i][j] += a; }
    }
  }
  void operator/=(const T a) {
    assert(a != 0);
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { this->data_[i][j] /= a; }
    }
  }
  void operator*=(const T a) {
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { this->data_[i][j] *= a; }
    }
  }
  PCCVector2<T> operator*(const PCCVector2<T>& rhs) const {
    PCCVector2<T> res;
    for (int i = 0; i < 2; ++i) {
      res[i] = 0;
      for (int j = 0; j < 2; ++j) { res[i] += this->data_[i][j] * rhs[j]; }
    }
    return res;
  }
  PCCMatrix2 operator*(const PCCMatrix2& rhs) const {
    PCCMatrix2<T> res;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) {
        res.data_[i][j] = 0;
        for (int k = 0; k < 2; ++k) { res.data_[i][j] += this->data_[i][k] * rhs.data_[k][j]; }
      }
    }
    return res;
  }
  PCCMatrix2 operator+(const PCCMatrix2& rhs) const {
    PCCMatrix2<T> res;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { res.data_[i][j] = this->data_[i][j] + rhs.data_[i][j]; }
    }
    return res;
  }
  PCCMatrix2 operator-(const PCCMatrix2& rhs) const {
    PCCMatrix2<T> res;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { res.data_[i][j] = this->data_[i][j] - rhs.data_[i][j]; }
    }
    return res;
  }
  PCCMatrix2 operator-() const {
    PCCMatrix2<T> res;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { res.data_[i][j] = -this->data_[i][j]; }
    }
    return res;
  }
  PCCMatrix2 operator*(T rhs) const {
    PCCMatrix2<T> res;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { res.data_[i][j] = this->data_[i][j] * rhs; }
    }
    return res;
  }
  PCCMatrix2 operator/(T rhs) const {
    assert(rhs != 0);
    PCCMatrix2<T> res;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { res.data_[i][j] = this->data_[i][j] / rhs; }
    }
    return res;
  }
  PCCMatrix2 transpose() const {
    PCCMatrix2<T> res;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { res.data_[i][j] = this->data_[j][i]; }
    }
    return res;
  }
  PCCMatrix2() = default;
  PCCMatrix2(const T a) {
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { data_[i][j] = a; }
    }
  }
  PCCMatrix2(const PCCMatrix2& rhs) { memcpy(data_, rhs.data_, sizeof(data_)); }
  ~PCCMatrix2(void) = default;

  friend inline PCCMatrix2<T> operator*(T lhs, const PCCMatrix2<T>& rhs) {
    PCCMatrix2<T> res;
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) { res.data_[i][j] = lhs * rhs.data_[i][j]; }
    }
    return res;
  }
  friend std::ostream& operator<<(std::ostream& os, const PCCMatrix2<T>& mat) {
    os << mat[0][0] << "," << mat[0][1]  << std::endl;
    os << mat[1][0] << "," << mat[1][1]  << std::endl;
    return os;
  }

  static void makeIdentity(PCCMatrix2<T>& mat) {
    memset(mat.data_, 0, sizeof(mat.data_));
    for (int i = 0; i < 2; ++i) { mat[i][i] = 1; }
  }

  static void makeScale(const T sx, const T sy, const T sz, PCCMatrix2<T>& mat) {
    makeIdentity(mat);
    mat[0][0] = sx;
    mat[1][1] = sy;
  }
  static void makeUniformScale(const T s, PCCMatrix2<T>& mat) { makeScale(s, s, s, mat); }

private:
  T data_[2][2];
};

//!    3x3 Matrix
template <typename T>
class PCCMatrix3 {
 public:
  T* operator[]( const size_t rowIndex ) {
    assert( rowIndex < 3 );
    return data_[rowIndex];
  }
  const T* operator[]( const size_t rowIndex ) const {
    assert( rowIndex < 3 );
    return data_[rowIndex];
  }
  size_t      getColumnCount() const { return 3; }
  size_t      getRowCount() const { return 3; }
  PCCMatrix3& operator=( const PCCMatrix3& rhs ) {
    memcpy( data_, rhs.data_, sizeof( data_ ) );
    return *this;
  }
  void operator+=( const PCCMatrix3& rhs ) {
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { this->data_[i][j] += rhs.data_[i][j]; }
    }
  }
  void operator-=( const PCCMatrix3& rhs ) {
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { this->data_[i][j] -= rhs.data_[i][j]; }
    }
  }
  void operator-=( const T a ) {
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { this->data_[i][j] -= a; }
    }
  }
  void operator+=( const T a ) {
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { this->data_[i][j] += a; }
    }
  }
  void operator/=( const T a ) {
    assert( a != 0 );
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { this->data_[i][j] /= a; }
    }
  }
  void operator*=( const T a ) {
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { this->data_[i][j] *= a; }
    }
  }
  PCCVector3<T> operator*( const PCCVector3<T>& rhs ) const {
    PCCVector3<T> res;
    for ( int i = 0; i < 3; ++i ) {
      res[i] = 0;
      for ( int j = 0; j < 3; ++j ) { res[i] += this->data_[i][j] * rhs[j]; }
    }
    return res;
  }
  PCCMatrix3 operator*( const PCCMatrix3& rhs ) const {
    PCCMatrix3<T> res;
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) {
        res.data_[i][j] = 0;
        for ( int k = 0; k < 3; ++k ) { res.data_[i][j] += this->data_[i][k] * rhs.data_[k][j]; }
      }
    }
    return res;
  }
  PCCMatrix3 operator+( const PCCMatrix3& rhs ) const {
    PCCMatrix3<T> res;
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { res.data_[i][j] = this->data_[i][j] + rhs.data_[i][j]; }
    }
    return res;
  }
  PCCMatrix3 operator-( const PCCMatrix3& rhs ) const {
    PCCMatrix3<T> res;
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { res.data_[i][j] = this->data_[i][j] - rhs.data_[i][j]; }
    }
    return res;
  }
  PCCMatrix3 operator-() const {
    PCCMatrix3<T> res;
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { res.data_[i][j] = -this->data_[i][j]; }
    }
    return res;
  }
  PCCMatrix3 operator*( T rhs ) const {
    PCCMatrix3<T> res;
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { res.data_[i][j] = this->data_[i][j] * rhs; }
    }
    return res;
  }
  PCCMatrix3 operator/( T rhs ) const {
    assert( rhs != 0 );
    PCCMatrix3<T> res;
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { res.data_[i][j] = this->data_[i][j] / rhs; }
    }
    return res;
  }
  PCCMatrix3 transpose() const {
    PCCMatrix3<T> res;
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { res.data_[i][j] = this->data_[j][i]; }
    }
    return res;
  }
  PCCMatrix3() = default;
  PCCMatrix3( const T a ) {
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { data_[i][j] = a; }
    }
  }
  PCCMatrix3( const PCCMatrix3& rhs ) { memcpy( data_, rhs.data_, sizeof( data_ ) ); }
  ~PCCMatrix3( void ) = default;

  friend inline PCCMatrix3<T> operator*( T lhs, const PCCMatrix3<T>& rhs ) {
    PCCMatrix3<T> res;
    for ( int i = 0; i < 3; ++i ) {
      for ( int j = 0; j < 3; ++j ) { res.data_[i][j] = lhs * rhs.data_[i][j]; }
    }
    return res;
  }

  friend std::ostream& operator<<(std::ostream& os, const PCCMatrix3<T>& mat) {
    os << mat[0][0] << "," << mat[0][1] << "," << mat[0][2] << std::endl;
    os << mat[1][0] << "," << mat[1][1] << "," << mat[1][2] << std::endl;
    os << mat[2][0] << "," << mat[2][1] << "," << mat[2][2] << std::endl;
    return os;
  }

  static void makeIdentity( PCCMatrix3<T>& mat ) {
    memset( mat.data_, 0, sizeof( mat.data_ ) );
    for ( int i = 0; i < 3; ++i ) { mat[i][i] = 1; }
  }

  static void makeScale( const T sx, const T sy, const T sz, PCCMatrix3<T>& mat ) {
    makeIdentity( mat );
    mat[0][0] = sx;
    mat[1][1] = sy;
    mat[2][2] = sz;
  }
  static void makeUniformScale( const T s, PCCMatrix3<T>& mat ) { makeScale( s, s, s, mat ); }
  static void makeRotation( const T angle, const T ax, const T ay, const T az, PCCMatrix3<T>& mat ) {
    T c       = cos( angle );
    T l_c     = 1 - c;
    T s       = sin( angle );
    mat[0][0] = ax * ax + ( 1 - ax * ax ) * c;
    mat[0][1] = ax * ay * l_c - az * s;
    mat[0][2] = ax * az * l_c + ay * s;
    mat[1][0] = ax * ay * l_c + az * s;
    mat[1][1] = ay * ay + ( 1 - ay * ay ) * c;
    mat[1][2] = ay * az * l_c - ax * s;
    mat[2][0] = ax * az * l_c - ay * s;
    mat[2][1] = ay * az * l_c + ax * s;
    mat[2][2] = az * az + ( 1 - az * az ) * c;
  }

 private:
  T data_[3][3];
};

//!    4x4 Matrix
template <typename T>
class PCCMatrix4 {
public:
  T* operator[](const size_t rowIndex) {
    assert(rowIndex < 4);
    return data_[rowIndex];
  }
  const T* operator[](const size_t rowIndex) const {
    assert(rowIndex < 4);
    return data_[rowIndex];
  }
  size_t      getColumnCount() const { return 4; }
  size_t      getRowCount() const { return 4; }
  PCCMatrix4& operator=(const PCCMatrix4& rhs) {
    memcpy(data_, rhs.data_, sizeof(data_));
    return *this;
  }
  void operator+=(const PCCMatrix4& rhs) {
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { this->data_[i][j] += rhs.data_[i][j]; }
    }
  }
  void operator-=(const PCCMatrix4& rhs) {
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) { this->data_[i][j] -= rhs.data_[i][j]; }
    }
  }
  void operator-=(const T a) {
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { this->data_[i][j] -= a; }
    }
  }
  void operator+=(const T a) {
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { this->data_[i][j] += a; }
    }
  }
  void operator/=(const T a) {
    assert(a != 0);
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { this->data_[i][j] /= a; }
    }
  }
  void operator*=(const T a) {
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { this->data_[i][j] *= a; }
    }
  }
  PCCVector4<T> operator*(const PCCVector4<T>& rhs) const {
    PCCVector4<T> res;
    for (int i = 0; i < 4; ++i) {
      res[i] = 0;
      for (int j = 0; j < 4; ++j) { res[i] += this->data_[i][j] * rhs[j]; }
    }
    return res;
  }
  PCCMatrix4 operator*(const PCCMatrix4& rhs) const {
    PCCMatrix4<T> res;
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) {
        res.data_[i][j] = 0;
        for (int k = 0; k < 4; ++k) { res.data_[i][j] += this->data_[i][k] * rhs.data_[k][j]; }
      }
    }
    return res;
  }
  PCCMatrix4 operator+(const PCCMatrix4& rhs) const {
    PCCMatrix4<T> res;
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { res.data_[i][j] = this->data_[i][j] + rhs.data_[i][j]; }
    }
    return res;
  }
  PCCMatrix4 operator-(const PCCMatrix4& rhs) const {
    PCCMatrix4<T> res;
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { res.data_[i][j] = this->data_[i][j] - rhs.data_[i][j]; }
    }
    return res;
  }
  PCCMatrix4 operator-() const {
    PCCMatrix4<T> res;
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) { res.data_[i][j] = -this->data_[i][j]; }
    }
    return res;
  }
  PCCMatrix4 operator*(T rhs) const {
    PCCMatrix4<T> res;
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { res.data_[i][j] = this->data_[i][j] * rhs; }
    }
    return res;
  }
  PCCMatrix4 operator/(T rhs) const {
    assert(rhs != 0);
    PCCMatrix4<T> res;
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { res.data_[i][j] = this->data_[i][j] / rhs; }
    }
    return res;
  }
  PCCMatrix4 transpose() const {
    PCCMatrix4<T> res;
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { res.data_[i][j] = this->data_[j][i]; }
    }
    return res;
  }
  PCCMatrix4 invert() const {
    PCCMatrix4<T> res;
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { 
        if (this->data_[i][j] != 0)
          res.data_[i][j] = 1 / this->data_[i][j];
        else
          res.data_[i][j] = 0;
      }
    }
    return res;
  }
  PCCMatrix4() = default;
  PCCMatrix4(const T a) {
    for (int i = 0; i < 4; ++i) {
      for (int j = 0; j < 4; ++j) { data_[i][j] = a; }
    }
  }
  PCCMatrix4(const PCCMatrix4& rhs) { memcpy(data_, rhs.data_, sizeof(data_)); }
  void load(const int rowIndex, const int colIndex, const PCCMatrix3<T>& rhs) {
    assert(rowIndex >= 0);
    assert(rowIndex + 3 <= 4);
    assert(colIndex >= 0);
    assert(colIndex + 3 <= 4);
    for (int i = rowIndex; i < rowIndex +3; ++i) {
      for (int j = colIndex; j < colIndex+3; ++j) { data_[i][j] = rhs[i-rowIndex][j-colIndex]; }
    }; 
  }
  void load(const int rowIndex, const int colIndex, const PCCMatrix2<T>& rhs) {
    assert(rowIndex >= 0);
    assert(rowIndex + 2 <= 4);
    assert(colIndex >= 0);
    assert(colIndex + 2 <= 4);
    for (int i = rowIndex; i < rowIndex + 2; ++i) {
      for (int j = colIndex; j < colIndex + 2; ++j) { data_[i][j] = rhs[i - rowIndex][j - colIndex]; }
    };
  }
  void load(const int rowIndex, const int colIndex, const PCCVector3<T>& rhs) {
    assert(rowIndex >= 0);
    assert(rowIndex + 3 <= 4);
    assert(colIndex >= 0);
    assert(colIndex + 1 <= 4);
    for (int i = rowIndex; i < rowIndex + 3; ++i) {data_[i][colIndex] = rhs[i - rowIndex]; }
  }
  void load(const int rowIndex, const int colIndex, const PCCVector2<T>& rhs) {
    assert(rowIndex >= 0);
    assert(rowIndex + 2 <= 4);
    assert(colIndex >= 0);
    assert(colIndex + 1 <= 4);
    for (int i = rowIndex; i < rowIndex + 2; ++i) { data_[i][colIndex] = rhs[i - rowIndex]; }
  }
  ~PCCMatrix4(void) = default;

  friend inline PCCMatrix4<T> operator*(T lhs, const PCCMatrix4<T>& rhs) {
    PCCMatrix4<T> res;
    for (int i = 0; i < 3; ++i) {
      for (int j = 0; j < 3; ++j) { res.data_[i][j] = lhs * rhs.data_[i][j]; }
    }
    return res;
  }
  friend std::ostream& operator<<(std::ostream& os, const PCCMatrix4<T>& mat) {
    os << mat[0][0] << "," << mat[0][1] << "," << mat[0][2] << "," << mat[0][3] << std::endl;
    os << mat[1][0] << "," << mat[1][1] << "," << mat[1][2] << "," << mat[1][3] << std::endl;
    os << mat[2][0] << "," << mat[2][1] << "," << mat[2][2] << "," << mat[2][3] << std::endl;
    os << mat[3][0] << "," << mat[3][1] << "," << mat[3][2] << "," << mat[3][3] << std::endl;
    return os;
  }

  static void makeIdentity(PCCMatrix4<T>& mat) {
    memset(mat.data_, 0, sizeof(mat.data_));
    for (int i = 0; i < 4; ++i) { mat[i][i] = 1; }
  }
  static void makeScale(const T sx, const T sy, const T sz, const T sw, PCCMatrix3<T>& mat) {
    makeIdentity(mat);
    mat[0][0] = sx;
    mat[1][1] = sy;
    mat[2][2] = sz;
    mat[3][3] = sw;
  }
  static void makeUniformScale(const T s, PCCMatrix4<T>& mat) { makeScale(s, s, s, s, mat); }
private:
  T data_[4][4];
};

typedef PCCVector2<double>  PCCVector2D;
typedef PCCVector3<double>   PCCVector3D;
typedef PCCVector4<double>  PCCVector4D;
typedef PCCVector2<int16_t> PCCPoint2D;
typedef PCCVector3<int16_t>  PCCPoint3D;
typedef PCCVector4<int16_t> PCCPoint4D;

typedef PCCBox3<double>      PCCBox3D;
typedef PCCBox3<int16_t>     PCCInt16Box3D;
typedef PCCVector3<uint8_t>  PCCColor3B;
typedef PCCVector3<uint16_t> PCCColor16bit;
typedef PCCVector3<double>   PCCNormal3D;
typedef PCCMatrix2<double>  PCCMatrix2D;
typedef PCCMatrix3<double>   PCCMatrix3D;
typedef PCCMatrix4<double>  PCCMatrix4D;

static inline PCCVector2D operator+( const PCCVector2D& a, const PCCPoint2D& b ) {
  return PCCVector2D( a[0] + b[0], a[1] + b[1] );
}
static inline PCCVector2D operator+( const PCCPoint2D& a, const PCCVector2D& b ) {
  return PCCVector2D( a[0] + b[0], a[1] + b[1] );
}
static inline PCCVector2D operator-( const PCCVector2D& a, const PCCPoint2D& b ) {
  return PCCVector2D( a[0] - b[0], a[1] - b[1] );
}
static inline PCCVector2D operator-( const PCCPoint2D& a, const PCCVector2D& b ) {
  return PCCVector2D( a[0] - b[0], a[1] - b[1] );
}
static inline double operator*( const PCCVector2D& a, const PCCVector2D& b ) {
  return ( a[0] * b[0] + a[1] * b[1]  );
}
static inline double operator*( const PCCVector2D& a, const PCCPoint2D& b ) {
  return ( a[0] * b[0] + a[1] * b[1] );
}
static inline double operator*( const PCCPoint2D& a, const PCCVector2D& b ) {
  return ( a[0] * b[0] + a[1] * b[1]  );
}
static inline PCCVector2D& operator+=( PCCVector2D& a, const PCCPoint2D& b ) {
  a = a + b;
  return a;
}
static inline PCCVector2D& operator-=( PCCVector2D& a, const PCCPoint2D& b ) {
  a = a - b;
  return a;
}

static inline PCCVector3D operator+( const PCCVector3D& a, const PCCPoint3D& b ) {
  return PCCVector3D( a[0] + b[0], a[1] + b[1], a[2] + b[2] );
}
static inline PCCVector3D operator+( const PCCPoint3D& a, const PCCVector3D& b ) {
  return PCCVector3D( a[0] + b[0], a[1] + b[1], a[2] + b[2] );
}
static inline PCCVector3D operator-( const PCCVector3D& a, const PCCPoint3D& b ) {
  return PCCVector3D( a[0] - b[0], a[1] - b[1], a[2] - b[2] );
}
static inline PCCVector3D operator-( const PCCPoint3D& a, const PCCVector3D& b ) {
  return PCCVector3D( a[0] - b[0], a[1] - b[1], a[2] - b[2] );
}
static inline double operator*( const PCCVector3D& a, const PCCVector3D& b ) {
  return ( a[0] * b[0] + a[1] * b[1] + a[2] * b[2] );
}
static inline double operator*( const PCCVector3D& a, const PCCPoint3D& b ) {
  return ( a[0] * b[0] + a[1] * b[1] + a[2] * b[2] );
}
static inline double operator*( const PCCPoint3D& a, const PCCVector3D& b ) {
  return ( a[0] * b[0] + a[1] * b[1] + a[2] * b[2] );
}
static inline PCCVector3D& operator+=( PCCVector3D& a, const PCCPoint3D& b ) {
  a = a + b;
  return a;
}
static inline PCCVector3D& operator-=( PCCVector3D& a, const PCCPoint3D& b ) {
  a = a - b;
  return a;
}

static inline PCCVector4D operator+( const PCCVector4D& a, const PCCPoint4D& b ) {
  return PCCVector4D( a[0] + b[0], a[1] + b[1], a[2] + b[2], a[3] + b[3] );
}
static inline PCCVector4D operator+( const PCCPoint4D& a, const PCCVector4D& b ) {
  return PCCVector4D( a[0] + b[0], a[1] + b[1], a[2] + b[2], a[3] + b[3] );
}
static inline PCCVector4D operator-( const PCCVector4D& a, const PCCPoint4D& b ) {
  return PCCVector4D( a[0] - b[0], a[1] - b[1], a[2] - b[2], a[3] - b[3] );
}
static inline PCCVector4D operator-( const PCCPoint4D& a, const PCCVector4D& b ) {
  return PCCVector4D( a[0] - b[0], a[1] - b[1], a[2] - b[2], a[3] - b[3] );
}
static inline double operator*( const PCCVector4D& a, const PCCVector4D& b ) {
  return ( a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3] );
}
static inline double operator*( const PCCVector4D& a, const PCCPoint4D& b ) {
  return ( a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3] );
}
static inline double operator*( const PCCPoint4D& a, const PCCVector4D& b ) {
  return ( a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3] );
}
static inline PCCVector4D& operator+=( PCCVector4D& a, const PCCPoint4D& b ) {
  a = a + b;
  return a;
}
static inline PCCVector4D& operator-=( PCCVector4D& a, const PCCPoint4D& b ) {
  a = a - b;
  return a;
}

// Slightly modified version of http://www.melax.com/diag.html?attredirects=0
// A must be a symmetric matrix.
// returns Q and D such that
// Diagonal matrix D = QT * A * Q;  and  A = Q*D*QT
static inline void PCCDiagonalize( const PCCMatrix3<double>& A, PCCMatrix3<double>& Q, PCCMatrix3<double>& D ) {
  const int          maxsteps = 24;  // certainly wont need that many.
  int                k0, k1, k2;
  double             o[3], m[3];
  double             q[4] = {0.0, 0.0, 0.0, 1.0};
  double             jr[4];
  double             sqw, sqx, sqy, sqz;
  double             tmp1, tmp2, mq;
  PCCMatrix3<double> AQ;
  double             thet, sgn, t, c;
  for ( int i = 0; i < maxsteps; ++i ) {
    // quat to matrix
    sqx     = q[0] * q[0];
    sqy     = q[1] * q[1];
    sqz     = q[2] * q[2];
    sqw     = q[3] * q[3];
    Q[0][0] = ( sqx - sqy - sqz + sqw );
    Q[1][1] = ( -sqx + sqy - sqz + sqw );
    Q[2][2] = ( -sqx - sqy + sqz + sqw );
    tmp1    = q[0] * q[1];
    tmp2    = q[2] * q[3];
    Q[1][0] = 2.0 * ( tmp1 + tmp2 );
    Q[0][1] = 2.0 * ( tmp1 - tmp2 );
    tmp1    = q[0] * q[2];
    tmp2    = q[1] * q[3];
    Q[2][0] = 2.0 * ( tmp1 - tmp2 );
    Q[0][2] = 2.0 * ( tmp1 + tmp2 );
    tmp1    = q[1] * q[2];
    tmp2    = q[0] * q[3];
    Q[2][1] = 2.0 * ( tmp1 + tmp2 );
    Q[1][2] = 2.0 * ( tmp1 - tmp2 );

    // AQ = A * Q;
    AQ[0][0] = Q[0][0] * A[0][0] + Q[1][0] * A[0][1] + Q[2][0] * A[0][2];
    AQ[0][1] = Q[0][1] * A[0][0] + Q[1][1] * A[0][1] + Q[2][1] * A[0][2];
    AQ[0][2] = Q[0][2] * A[0][0] + Q[1][2] * A[0][1] + Q[2][2] * A[0][2];
    AQ[1][0] = Q[0][0] * A[0][1] + Q[1][0] * A[1][1] + Q[2][0] * A[1][2];
    AQ[1][1] = Q[0][1] * A[0][1] + Q[1][1] * A[1][1] + Q[2][1] * A[1][2];
    AQ[1][2] = Q[0][2] * A[0][1] + Q[1][2] * A[1][1] + Q[2][2] * A[1][2];
    AQ[2][0] = Q[0][0] * A[0][2] + Q[1][0] * A[1][2] + Q[2][0] * A[2][2];
    AQ[2][1] = Q[0][1] * A[0][2] + Q[1][1] * A[1][2] + Q[2][1] * A[2][2];
    AQ[2][2] = Q[0][2] * A[0][2] + Q[1][2] * A[1][2] + Q[2][2] * A[2][2];

    // D  = Q.transpose() * AQ;
    D[0][0] = AQ[0][0] * Q[0][0] + AQ[1][0] * Q[1][0] + AQ[2][0] * Q[2][0];
    D[0][1] = AQ[0][0] * Q[0][1] + AQ[1][0] * Q[1][1] + AQ[2][0] * Q[2][1];
    D[0][2] = AQ[0][0] * Q[0][2] + AQ[1][0] * Q[1][2] + AQ[2][0] * Q[2][2];
    D[1][0] = AQ[0][1] * Q[0][0] + AQ[1][1] * Q[1][0] + AQ[2][1] * Q[2][0];
    D[1][1] = AQ[0][1] * Q[0][1] + AQ[1][1] * Q[1][1] + AQ[2][1] * Q[2][1];
    D[1][2] = AQ[0][1] * Q[0][2] + AQ[1][1] * Q[1][2] + AQ[2][1] * Q[2][2];
    D[2][0] = AQ[0][2] * Q[0][0] + AQ[1][2] * Q[1][0] + AQ[2][2] * Q[2][0];
    D[2][1] = AQ[0][2] * Q[0][1] + AQ[1][2] * Q[1][1] + AQ[2][2] * Q[2][1];
    D[2][2] = AQ[0][2] * Q[0][2] + AQ[1][2] * Q[1][2] + AQ[2][2] * Q[2][2];

    o[0] = D[1][2];
    o[1] = D[0][2];
    o[2] = D[0][1];
    m[0] = fabs( o[0] );
    m[1] = fabs( o[1] );
    m[2] = fabs( o[2] );

    k0 = ( m[0] > m[1] && m[0] > m[2] ) ? 0 : ( m[1] > m[2] ) ? 1 : 2;  // index of largest element of offdiag
    k1 = ( k0 + 1 ) % 3;
    k2 = ( k0 + 2 ) % 3;
    if ( o[k0] == 0.0 ) {
      break;  // diagonal already
    }
    thet = ( D[k2][k2] - D[k1][k1] ) / ( 2.0 * o[k0] );
    sgn  = ( thet > 0.0 ) ? 1.0 : -1.0;
    thet *= sgn;                                                                  // make it positive
    t = sgn / ( thet + ( ( thet < 1.E6 ) ? sqrt( thet * thet + 1.0 ) : thet ) );  // sign(T)/(|T|+sqrt(T^2+1))
    c = 1.0 / sqrt( t * t + 1.0 );                                                //  c= 1/(t^2+1) , t=s/c
    if ( c == 1.0 ) {
      break;  // no room for improvement - reached machine precision.
    }
    jr[0] = jr[1] = jr[2] = jr[3] = 0.0;
    jr[k0]                        = sgn * sqrt( ( 1.0 - c ) / 2.0 );  // using 1/2 angle identity sin(a/2)
                                                                      // = sqrt((1-cos(a))/2)
    jr[k0] *= -1.0;  // since our quat-to-matrix convention was for v*M instead of M*v
    jr[3] = sqrt( 1.0 - jr[k0] * jr[k0] );
    if ( jr[3] == 1.0 ) {
      break;  // reached limits of floating point precision
    }
    q[0] = ( q[3] * jr[0] + q[0] * jr[3] + q[1] * jr[2] - q[2] * jr[1] );
    q[1] = ( q[3] * jr[1] - q[0] * jr[2] + q[1] * jr[3] + q[2] * jr[0] );
    q[2] = ( q[3] * jr[2] + q[0] * jr[1] - q[1] * jr[0] + q[2] * jr[3] );
    q[3] = ( q[3] * jr[3] - q[0] * jr[0] - q[1] * jr[1] - q[2] * jr[2] );
    mq   = sqrt( q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3] );
    q[0] /= mq;
    q[1] /= mq;
    q[2] /= mq;
    q[3] /= mq;
  }
}

template <typename T>
T PCCClip( const T& n, const T& lower, const T& upper ) {
  return ( std::max )( lower, ( std::min )( n, upper ) );
}
template <typename T>
bool PCCApproximatelyEqual( T a, T b, T epsilon = std::numeric_limits<PCCType>::epsilon() ) {
  return fabs( a - b ) <= ( ( fabs( a ) < fabs( b ) ? fabs( b ) : fabs( a ) ) * epsilon );
}

struct Quaternion
{
    double w, x, y, z;
};

struct EulerAngles {
    double roll, pitch, yaw;
};

static inline Quaternion& ToQuaternion(EulerAngles euler){
  //from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
  // the angles are coming in degrees, and converted to radians in this function
	double radperdeg = 0.01745329251994329576923690768489F; //could we have this in fixed point????
  double yaw = radperdeg * euler.yaw;
  double pitch = radperdeg * euler.pitch;
  double roll = radperdeg * euler.roll;
    // Abbreviations for the various angular functions
    double cy = cos(yaw * 0.5);
    double sy = sin(yaw * 0.5);
    double cp = cos(pitch * 0.5);
    double sp = sin(pitch * 0.5);
    double cr = cos(roll * 0.5);
    double sr = sin(roll * 0.5);

    Quaternion q;
    q.w = cy * cp * cr + sy * sp * sr;
    q.x = cy * cp * sr - sy * sp * cr;
    q.y = sy * cp * sr + cy * sp * cr;
    q.z = sy * cp * cr - cy * sp * sr;

    return q;
}

static inline EulerAngles& ToEulerAngles(Quaternion q) {
  //from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
  EulerAngles angles;
  double degperrad = 57.295779513082320876798154814092F;

  // roll (x-axis rotation)
  double sinr_cosp = 2 * (q.w * q.x + q.y * q.z);
  double cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y);
  angles.roll = std::atan2(sinr_cosp, cosr_cosp);
  angles.roll *= degperrad; // angles in degrees

  // pitch (y-axis rotation)
  double sinp = 2 * (q.w * q.y - q.z * q.x);
  if (std::abs(sinp) >= 1)
    angles.pitch = std::copysign(M_PI / 2, sinp); // use 90 degrees if out of range
  else
    angles.pitch = std::asin(sinp);
  angles.pitch *= degperrad; // angles in degrees

  // yaw (z-axis rotation)
  double siny_cosp = 2 * (q.w * q.z + q.x * q.y);
  double cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z);
  angles.yaw = std::atan2(siny_cosp, cosy_cosp);
  angles.yaw *= degperrad; // angles in degrees

  return angles;
}

static inline void ToRotationMatrix(Quaternion q, PCCMatrix3D& rotMatrix) {
  //from https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
  rotMatrix[0][0] = 1 - 2 * (q.y * q.y + q.z * q.z);  // R_xx
  rotMatrix[0][1] = 2 * (q.x * q.y - q.z * q.w);      // R_xy
  rotMatrix[0][2] = 2 * (q.z * q.x + q.y * q.w);      // R_xz
  rotMatrix[1][0] = 2 * (q.x * q.y + q.z * q.w);      // R_yx
  rotMatrix[1][1] = 1 - 2 * (q.z * q.z + q.x * q.x);  // R_yy
  rotMatrix[1][2] = 2 * (q.y * q.z - q.x * q.w);      // R_yz
  rotMatrix[2][0] = 2 * (q.z * q.x - q.y * q.w);      // R_zx
  rotMatrix[2][1] = 2 * (q.y * q.z + q.x * q.w);      // R_zy
  rotMatrix[2][2] = 1 - 2 * (q.x * q.x + q.y * q.y);  // R_zz
}

}  // namespace pcc

#endif /* PCCMath_h */
